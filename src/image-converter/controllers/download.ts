import {publicConverter} from '@/image-converter/services/converter';
import {ImageStatus} from '@/packages/image-converter';
import {Request, Response} from 'express';

export const download = async (req: Request, res: Response) => {
  const image = publicConverter.pop(req.params.id);

  if (image.status === ImageStatus.CONVERTED) {
    return res.download(image.filepath!);
  }

  return res.status(404).end();
}
