import {publicConverter} from '@/image-converter/services/converter';
import {Request, Response} from 'express';

export const imageStatus = (req: Request, res: Response) => {
  const status = publicConverter.get(req.params.id).status;
  res.json({status});
}
