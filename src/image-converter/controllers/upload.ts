import {publicConverter} from '@/image-converter/services/converter';
import {Request, Response} from 'express';

export const upload = (req: Request, res: Response) => {
  const imageId = publicConverter.add({
    filepath: req.file!.path,
    options: {
      quality: +req.body.quality,
      targetFormat: req.body.targetFormat
    }
  })
  res.json({imageId});
}
