import {download} from '@/image-converter/controllers/download';
import {imageStatus} from '@/image-converter/controllers/image-status';
import {upload} from '@/image-converter/controllers/upload';
import {UploadValidationSchema} from '@/image-converter/validation/upload';
import {imageUploadMiddleware} from '@/packages/upload';
import express from 'express';
const validator = require('express-joi-validation').createValidator({})

const imageConverterRouter = express.Router();

imageConverterRouter.post('/upload', [validator.body(UploadValidationSchema), imageUploadMiddleware], upload)
imageConverterRouter.get('/imageStatus/:id', imageStatus)
imageConverterRouter.get('/download/:id', download);

export {imageConverterRouter};
