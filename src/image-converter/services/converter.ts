import {CONVERTED_IMAGES_DIR} from '@/env-init';
import {createImageConverter} from '@/packages/image-converter';

export const publicConverter = createImageConverter(CONVERTED_IMAGES_DIR!, process.env.MAX_IMAGES_IN_TIME!);
