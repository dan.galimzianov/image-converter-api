import Joi from 'joi';

export const UploadValidationSchema = Joi.object({
  quality: Joi.number().required(),
  targetFormat: Joi.string().required(),
  lossless: Joi.boolean()
})
