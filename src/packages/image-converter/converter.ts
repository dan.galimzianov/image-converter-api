import {createStorage} from '@/packages/image-converter/images-storage';
import {importWorker} from '@/packages/import-worker';
import path from 'node:path';

const createImageConverter = (output: string, maxImagesInTime: string) => {
  const converterWorker = importWorker(path.join(__dirname, './worker-converter.ts'), {env: {MAX_IMAGES_IN_TIME: maxImagesInTime}});

  const processImage = (imageID, image) => {
    converterWorker.postMessage([imageID, image]);
  }

  const storage = createStorage(output, processImage);

  converterWorker.on('message', async ([imageID, status]) => {
    storage.updateStatus(imageID, status);
  });

  return {
    get: storage.get,
    pop: storage.pop,
    add: storage.add
  };
}

export {createImageConverter};
