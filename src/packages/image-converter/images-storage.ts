import {InputImage, ImageStatus, StorageImage} from '@/packages/image-converter/types';
import {randomUUID} from 'crypto';
import path from 'node:path';

const newImagePath = (imageInputPath, imageOutputDir, extension) => {
  const basename = path.basename(imageInputPath, path.extname(imageInputPath))
  return path.join(path.resolve(imageOutputDir), `${basename}.${extension}`)
}

const images = new Map<string, StorageImage>();

const createStorage = (outputDir: string, process: (imageID: string, imageData: StorageImage) => void) => {
  const add = (inputImage: InputImage) => {
    const imageID = randomUUID();
    const image: StorageImage = {
      ...inputImage,
      status: ImageStatus.IN_PROGRESS,
      outputFilepath: newImagePath(inputImage.filepath, outputDir, inputImage.options.targetFormat)
    };

    images.set(imageID, image);
    process(imageID, image)

    return imageID;
  }

  const get = (imageID: string): StorageImage => {
    return images.get(imageID) || {status: ImageStatus.NOT_FOUND} as StorageImage
  }

  const pop = (imageID: string): StorageImage => {
    const image = get(imageID);
    images.delete(imageID);
    return image;
  }

  const updateStatus = (imageID: string, status: ImageStatus) => {
    images.set(imageID, {...images.get(imageID)!, status})
  }

  return {add, get, pop, updateStatus}
}

export {createStorage}
