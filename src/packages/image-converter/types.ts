export interface InputImage {
  filepath: string;
  options: {
    targetFormat: string;
    quality?: number;
  }
}


export interface StorageImage extends InputImage {
  outputFilepath?: string;
  status: ImageStatus;
}

export enum ImageStatus {
  CONVERTED = 'CONVERTED',
  IN_PROGRESS = 'IN_PROGRESS',
  NOT_FOUND = 'NOT_FOUND',
  ERROR = 'ERROR'
}
