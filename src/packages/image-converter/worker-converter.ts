import {ImageStatus, StorageImage} from '@/packages/image-converter/types';
import path from 'node:path';
import sharp from 'sharp';
import {parentPort} from 'worker_threads';

const MAX_IMAGES_IN_TIME = Number(process.env.MAX_IMAGES_IN_TIME);
const unconvertedImages = new Map<string, StorageImage>();
let processingImagesCount = 0;

const imageConverter = async (imageID, image: StorageImage) => {
  let status;

  try {
    await sharp(image.filepath)[image.options.targetFormat](image.options).toFile(image.outputFilepath);
    status = ImageStatus.CONVERTED;
    console.log(`Image ${path.basename(image.filepath)} converted to ${path.basename(image.outputFilepath!)} with options: ${JSON.stringify(image.options)}`);
  } catch {
    status = ImageStatus.ERROR;
    console.log(`Cannot convert file ${path.basename(image.outputFilepath!)}`);
  }

  return status
}

const runConverter = () => {
  if (processingImagesCount >= MAX_IMAGES_IN_TIME || unconvertedImages.size === 0) {
    return;
  }

  const [imageID, imageData] = unconvertedImages.entries().next().value;
  processingImagesCount++;

  imageConverter(imageID, imageData)
    .then((status) => {
      parentPort?.postMessage([imageID, status]);
      unconvertedImages.delete(imageID);
      processingImagesCount--;
      setTimeout(runConverter);
    });
}

parentPort?.on("message", async ([imageID, imageData]: [string, StorageImage]) => {
  unconvertedImages.set(imageID, imageData);
  runConverter();
});
